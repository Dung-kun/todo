import { NgModule, Component } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule, Routes, CanActivateChild, CanDeactivate } from '@angular/router';
import { HomeComponent } from './../components/home/home.component';
import { DetailProductComponent } from './../components/product-component/detail-product/detail-product.component';
import { ProductListComponent } from './../components/product-component/product-list/product-list.component';
import { ProductGuard } from '../guards/product.guard';
import { ProductEditComponent } from '../components/product-component/product-edit/product-edit.component';


const productRoutes: Routes = [
  {
    path: "",
    component: ProductListComponent
  },
  {
    path: ':id',
    canActivateChild: [ProductGuard],
    children: [
      {
        path: '',
        component: DetailProductComponent
      },
      {
        path: 'edit',
        component: ProductEditComponent,
        canDeactivate: [ProductGuard]
      },
    ]
  },
  {
    path: "**",
    component: HomeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoutes),
    CommonModule
  ],
  exports: [RouterModule]
})

export class ProductRoutingModule {}
