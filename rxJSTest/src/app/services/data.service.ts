import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay,shareReplay, map, take } from 'rxjs/operators';
import { Product } from './../models/product.model';
import { User } from '../models/user.model';

const products : Product[] = [
  {
    id: '1',
    name: 'Ny',
    content: 'Chờ tiếp đi chứ cũng k có đâu =)))',
  },
  {
    id: '2',
    name: 'Sữa',
    content: 'Sữa rất tuyệt vời'
  },
  {
    id: '3',
    name: 'Ricado Milos',
    content: 'Điệu nhảy tuyệt vời :))'
  }
  ,
  {
    id: '4',
    name: 'Drama',
    content: 'Ở đâu cũng có'
  }
];

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }
  getProducts(): Observable<Product[]> {
    return of(products).pipe(delay(500)).pipe(shareReplay(1));
  }

  getProductWithId(idx: string): Observable<Product>{
    const tempProduct = products.find(x => x.id === idx);
    if(tempProduct) {
      return of(tempProduct).pipe(delay(2000));
    } else {
      return of(products[0])
    }
  }
  getUser(): Observable<User> {
    return of({
      id: '1',
      name: "Dũng kun",
      idProduct: "2"
    })
  }
}
