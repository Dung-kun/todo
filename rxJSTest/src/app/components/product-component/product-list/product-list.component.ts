import { Component, OnInit } from '@angular/core';
import { DataService } from './../../../services/data.service';
import { Product } from './../../../models/product.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products$!: Observable<Product[]>;
  constructor(private readonly service: DataService, private route: Router) { }

  ngOnInit(): void {
    this.products$ = this.service.getProducts();
  }
  handleClick(id: string)
  {
    this.route.navigate(['/productList', id]);
  }

  handleEdit(id: string){
    this.route.navigate(['productList', id, 'edit']);
  }
}
