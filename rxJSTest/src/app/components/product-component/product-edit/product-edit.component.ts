import { Component, OnInit } from '@angular/core';

import { CheckDeactivate } from '../../../models/check-deactivate';
import { ActivatedRoute } from '@angular/router';
import { pluck, map, shareReplay, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Product } from '../../../models/product.model';
import { DataService } from '../../../services/data.service';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, CheckDeactivate {
  product$: Observable<string> = of("Chưa có gì");
  tempProduct$: Observable<string>= of("Chưa có gì");;
  buttonName: string = "Change";

  constructor(private route: ActivatedRoute, private readonly service: DataService) { }

  ngOnInit(): void {
    this.product$ = this.route.params.pipe(
      pluck('id'),
      switchMap((val) => this.service.getProductWithId(val)),
      map(val => val.name),
      shareReplay(1)
    );
    this.tempProduct$ = this.product$;
  }

  handleClick(){
    if(this.product$ === this.tempProduct$)
    {
      this.product$ = of("Đã bị thay đổi");
      this.buttonName = "Return"
    } else {
      this.product$ = this.tempProduct$;
      this.buttonName = "CHange"
    }
  }

  checkDeactivate(currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean  {
    if(this.product$ === this.tempProduct$) return true;
    else return false;
  }
}
