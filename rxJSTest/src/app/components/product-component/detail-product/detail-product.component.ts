import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from './../../../models/product.model';
import { Observable, of } from 'rxjs';
import { delay, pluck, switchMap, filter, takeUntil } from 'rxjs/operators';
import { DataService } from './../../../services/data.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree } from '@angular/router';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.scss']
})
export class DetailProductComponent implements OnInit {
  product$!: Observable<Product>;
  loading: Observable<boolean> = of(true);
  constructor(private readonly route: ActivatedRoute, private readonly service: DataService,private readonly route2: Router ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(console.log)
    this.product$ =  this.route.params.pipe(
      pluck('id'),
      switchMap((val) => this.service.getProductWithId(val))
    )
  }
}
