import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './../product-list/product-list.component';
import { DetailProductComponent } from './../detail-product/detail-product.component';
import { ProductRoutingModule } from './../../../route/product-routing.module';
import { FormsModule } from  '@angular/forms';
import { ProductComponent } from './product.component'
import { ProductEditComponent } from '../product-edit/product-edit.component';

@NgModule({
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule
  ],
  declarations: [ProductEditComponent,ProductListComponent, DetailProductComponent, ProductComponent],
})

export class ProductModule { }
