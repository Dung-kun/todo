import { Component, OnInit } from '@angular/core';
import { DataService } from './../../../services/data.service';
import { Product } from './../../../models/product.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  products$!: Observable<Product[]>;
  constructor(private readonly service: DataService, private route: Router) { }

  ngOnInit(): void {
    this.products$ = this.service.getProducts();
  }
  handleClick(id: string)
  {
    this.route.navigate(['/product', id]);
  }
}
