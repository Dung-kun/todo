import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { Product } from './../../models/product.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products$!: Observable<Product[]>;
  constructor(private readonly service: DataService, private route: Router) { }

  ngOnInit(): void {
    this.products$ = this.service.getProducts();
  }
}
