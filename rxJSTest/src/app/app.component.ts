import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service'
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, Route } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'rxJSTest';
  constructor(private readonly route: Router){}
  ngOnInit() {

    const observer = {
      next: (val:any) => console.log(val),
      error: (err: Error) => console.log(err),
      complete: () => console.log('complete')
    }
  }
  // handleClick(){
  //   this.route.navigate(['non']);
  // }
}
interface User {
  id: string;
  username: string;
  firstname: string;
  lastname: string;
}
