import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate, CanLoad } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProductComponent } from './components/product-component/product/product.component';
import { TestComponent } from './test.component';
import { ProductGuard } from './guards/product.guard';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'productList',
    loadChildren: () => import('./components/product-component/product/product.module').then(val => val.ProductModule),
    //anActivate: [ProductGuard],
    //canLoad: [ProductGuard]
  },
  {
    path: 'non',
    component: TestComponent,
    //canActivate: [ProductGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
