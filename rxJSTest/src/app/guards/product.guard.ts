import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route, UrlSegment, CanActivate, CanActivateChild, RouterStateSnapshot, UrlTree, CanDeactivate, CanLoad } from '@angular/router';
import { Observable, of, from } from 'rxjs';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';
import { CheckDeactivate } from '../models/check-deactivate';

@Injectable({
  providedIn: 'root'
})
export class ProductGuard implements CanActivate, CanActivateChild, CanDeactivate<CheckDeactivate>, CanLoad {
  constructor(private readonly service: DataService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.service.getUser().pipe(
      map(val => !!val)
    );
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    console.log(childRoute)
    const _id = childRoute.params;
    console.log(_id)
    if(!_id){
      return of(false)
    }
    return this.service.getUser().pipe(map(val => val.idProduct === _id['id']))
  }

  canDeactivate(component: CheckDeactivate, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.checkDeactivate(currentRoute, currentState, nextState);
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
    ): Observable<boolean> {
      return this.service.getUser().pipe(
        map(val => !!val)
      );
      return of(false);
  }
}
